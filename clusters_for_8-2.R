require(fitbitScraper)
require(stringr)
require(matrixStats)
setwd("C:/Users/Diogo/Desktop/Faculdade/Mestrado/2� Ano/Tese/5 - Codigo dos padroes/R Code")

option <- 1 #1 to check every new day and his warning, 2 just to see that new day graph
aproximation <- 2 #Previous  #2 - Mean of Energies  #3 - RepresentativeDay Energy   #4 - Gaps  #EnergyGaps

if( exists("helper") && option == 1 && helper == 2) {
  rm(list=ls())
  option <- 1
}

#Parameters
newDayWished <- 4 #Just works with option 2.
recalculatedClusterDays <- 28 #How many indexes starting training data will have
daysToRecalculateCluster <- 7 #0 makes no update onto clusters
clusters <- 2 #Number of clusters
maxFactor <- 1.25
sdFactor <- 1
timeFactor <- 15 #15 15
lightRange <- 3 #4 Aproximity
seriousRange <- 21 #4 Aproximity
a23constantBig <- 2 #2 and 3 Aproximity
a23constantLittle <- 0.8 #2 and 3 Aproximity
a45constantUp <- 0.9 #4 and 5 Aproximity
a45constantDown <- 0.8 #4 and 5 Aproximity


#Related to TimeFactor
totalMinutes <- 1440
lagTotal <- 240
minutesDivisions <- totalMinutes / timeFactor
sumHowManyColumns <- timeFactor / 15
#lagFactor <- (lagTotal/timeFactor)/2
lagFactor <- 1

require(zoo)
#Function to calculate distance with sliding between all days
Pair.dist4 <- function(t,s, lag= lagFactor ) {
  B8s <- lag(zoo(s),-lag:lag , na.pad=T)
  k <- data.frame(abs(t-B8s))
  keepdist <- colSums(!is.na(k))
  keepd2 <- (colSums(k , na.rm=T))
  dists<-(keepd2/keepdist)
  mind<-min(dists)
  obstemp <- keepdist[mind==dists]
  if (length(obstemp)>1) {
    obstemp = sort(obstemp , decreasing = T)[1]
  }
  return(c(Dist=mind,Obs=obstemp,Lag=names(obstemp)))
}

#Medoid Day Representative Function
#Ward.clust == # indicates the cluster memberships of your cluster analysis.
#The result will be the medoid, representative day for the specified #cluster
med <- function(members,Dist){
  if(length(members)==1){return(members)}
  else{
    if(length(members)==0){return(0)}
    dists<-apply(Dist[members,members],1,sum)
    medoid<-members[which(dists==min(dists))]
    return(medoid[1])
  }
}

#1 aproximation
graphShow1 <- function(Final.Data,ward.clust,clusterDay,clusterWarning,meanRepresentativeDay,colors,newDay) {
  print(str_c("O novo dia pertence ao cluster " ,clusterDay, " e eh considerado " , clusterWarning, colapse=""))
  #for (i in 1:clusters) { 
  x11(width=13, height=8)
  par(mfrow=c(1,1)) #ANDKJSADNASKJDAKJDANKDNADNKJ
  matplot(x = 1:minutesDivisions ,Final.Data[,ward.clust==clusterDay], ylim=c(0,max(Final.Data)), type = "s",col =
            ward.clust[ward.clust==clusterDay], lty = 1 , main = str_c(clusterDay, " - ", clusterWarning)  , xlab = "15 Minute Intervals" ,
          ylab = "# of Steps")
  matlines(meanRepresentativeDay[[clusterDay]], x = 1:minutesDivisions , type = "l" , col = colors[clusterDay] , lwd = 5)
  matlines(newDay, x = 1:minutesDivisions , type = "l" , col = "blue" , lwd = 5)
  #1:minutesDivisions
}

# #2 and #3 aproximation
graphShow23 <- function(Final.Data,ward.clust,clusterDay,clusterWarning,representativeDay,colors,newDay,maxClusters,meanClusters,sdClusters,a23constantBig,a23constantLittle) {
  print(str_c("O novo dia pertence ao cluster " ,clusterDay, " e eh considerado " , clusterWarning, colapse=""))
  day <- c()
  lightProblemAbove <- c()
  seriousProblemAbove <- c()
  lightProblemUnder <- c()
  seriousProblemUnder <- c()
  standardDay <- c()

  for (i in 1:3) {
    day <- c(day,sum(newDay^2))
    lightProblemAbove <- c(lightProblemAbove,(meanClusters[clusterDay] + a23constantLittle*sdClusters[clusterDay]))
    seriousProblemAbove <- c(seriousProblemAbove,(meanClusters[clusterDay] + a23constantBig*sdClusters[clusterDay]))
    lightProblemUnder <- c(lightProblemUnder,(meanClusters[clusterDay] - a23constantLittle*sdClusters[clusterDay]))
    seriousProblemUnder <- c(seriousProblemUnder,(meanClusters[clusterDay] - a23constantBig*sdClusters[clusterDay]))
    standardDay <- c(standardDay,meanClusters[[clusterDay]])
  }  
  
  lightProblemUnder[lightProblemUnder < 0] <- 0
  seriousProblemUnder[seriousProblemUnder < 0] <- 0
    
  x11(width=13, height=8)
  par(mfrow=c(1,1)) #ANDKJSADNASKJDAKJDANKDNADNKJ
  matplot(x = 1:length(day), day , ylim=c(0,max(seriousProblemAbove[1],newDay[1])), type = "s",col =
            "blue", lty = 1 , main = str_c(clusterDay, " - ", clusterWarning)  , xlab = "Single Point" ,
          ylab = "Total Energy", lwd = 3)
  matlines(standardDay, x = 1:length(day) , type = "s" , col = "black" , lwd = 3)
  matlines(seriousProblemAbove, x = 1:length(day) , type = "s" , col = "red" , lwd = 5)
  matlines(seriousProblemUnder, x = 1:length(day) , type = "s" , col = "red" , lwd = 5)
  matlines(lightProblemAbove, x = 1:length(day) , type = "s" , col = "yellow" , lwd = 5)
  matlines(lightProblemUnder, x = 1:length(day) , type = "s" , col = "yellow" , lwd = 5)
  #1:minutesDivisionsmatlines(meanRepresentativeDay[[clusterDay]], x = 1:length(day) , type = "l" , col = "black" , lwd = 5)
}

#4 aproximation
graphShow45 <- function(Final.Data,sd.Data,clusterDay,clusterWarning,newDay,correctUp,correctDown,superior,inferior,meanRepresentativeDay) {
  print(str_c("O novo dia pertence ao cluster " ,clusterDay, "e eh considerado " , clusterWarning, colapse=""))
  x11(width=13, height=8)
  par(mfrow=c(1,1)) #ANDKJSADNASKJDAKJDANKDNADNKJ
  matplot(x = 1:length(newDay),newDay, ylim=c(0,max(correctUp,newDay)), type = "l",col =
            "blue", lty = 1 , main = str_c(clusterDay , " - " ,clusterWarning, " - " , superior , " - ", inferior)  , xlab = "15 Minute Intervals" ,
          ylab = "# of Steps", lwd = 3)
  matlines(t(correctUp), x = 1:length(correctUp) , type = "l" , col = "red" , lwd = 2)
  matlines(t(correctDown), x = 1:length(correctDown) , type = "l" , col = "red" , lwd = 2)
  matlines(t(meanRepresentativeDay[[clusterDay]]), x = 1:length(meanRepresentativeDay[[clusterDay]]) , type = "l" , col = "black" , lwd = 2)
  abline(0,0,lwd = 2)
}

#DON'T FORGET -> activity_new days column is 97 
if(!exists("newDayIndex")) {
  Meyer_2016 <-
    read.csv("./Meyer_2016.csv",header=T)
  Final.Data <- rollapply( t(Meyer_2016[c(1:recalculatedClusterDays),-c(97:100)]) , width = sumHowManyColumns , by = sumHowManyColumns , align = "left" , FUN = sum )
  newDays.Data <- rollapply( t(Meyer_2016[c((recalculatedClusterDays+1):nrow(Meyer_2016)),-c(97:100)]) , width = sumHowManyColumns , by = sumHowManyColumns , align = "left" , FUN = sum )
  dayNames <- c("Sunday","Monday", "Tuesday", "Wednesday", 
                "Thursday", "Friday", "Saturday")
  if (aproximation == 2 || aproximation == 3 || aproximation == 5) {
    if(aproximation == 5) {
      Final.Data <- Final.Data^2
      newDays.Data <- newDays.Data^2
    } else {
    ap2.Data <- matrix(colSums(Final.Data^2),1,ncol(Final.Data)) 
    ndap2.Data <- matrix(colSums(newDays.Data^2),1,ncol(newDays.Data))
    }
  }
  
  days <- factor(t(Meyer_2016[(1:recalculatedClusterDays),c(99)]), levels = dayNames,
                 ordered = TRUE)
  Final.DataWD <- rbind(Final.Data,days)
  newDayIndex <- 0
  auxToNewDay <- 1
  newDay <- c()
}

#Recalculates cluster
if(exists("newDayIndex") & newDayIndex == daysToRecalculateCluster & daysToRecalculateCluster != 0) {
  Final.Data <- rollapply( t(Meyer_2016[c((auxToNewDay):(recalculatedClusterDays+auxToNewDay-1)),-c(97:100)]) , width = sumHowManyColumns , by = sumHowManyColumns , align = "left" , FUN = sum )
  newDays.Data <- rollapply( t(Meyer_2016[c((recalculatedClusterDays+auxToNewDay):nrow(Meyer_2016)),-c(97:100)]) , width = sumHowManyColumns , by = sumHowManyColumns , align = "left" , FUN = sum )
  
  if (aproximation == 2 || aproximation == 3 || aproximation == 5) {
    if(aproximation == 5) {
      Final.Data <- Final.Data^2
      newDays.Data <- newDays.Data^2
    } else {
      ap2.Data <- matrix(colSums(Final.Data^2),1,ncol(Final.Data)) 
      ndap2.Data <- matrix(colSums(newDays.Data^2),1,ncol(newDays.Data))
    }
  }
  
  days <- factor(t(Meyer_2016[c((auxToNewDay):(recalculatedClusterDays+auxToNewDay-1)),c(99)]), levels = dayNames, ordered = TRUE)
  Final.DataWD <- rbind(Final.Data,days)
  
  newDayIndex <- 0
}

if(option == 2 && daysToRecalculateCluster != 0) {
  dayNames <- c("Sunday","Monday", "Tuesday", "Wednesday", 
                "Thursday", "Friday", "Saturday")
  newDay <- c()
  
  Meyer_2016 <-
    read.csv("./Meyer_2016.csv",header=T)
  Final.Data <- rollapply( t(Meyer_2016[c((trunc((newDayWished/daysToRecalculateCluster),1,2) * daysToRecalculateCluster+1):(recalculatedClusterDays + trunc((newDayWished/daysToRecalculateCluster),1,2) * daysToRecalculateCluster)),-c(97:100)]) , width = sumHowManyColumns , by = sumHowManyColumns , align = "left" , FUN = sum )
  newDays.Data <- rollapply( t(Meyer_2016[c((recalculatedClusterDays+1+ trunc((newDayWished/daysToRecalculateCluster),1,2) * daysToRecalculateCluster):nrow(Meyer_2016)),-c(97:100)]) , width = sumHowManyColumns , by = sumHowManyColumns , align = "left" , FUN = sum )
  
  if (aproximation == 2 || aproximation == 3 || aproximation == 5) {
    if(aproximation == 5) {
      Final.Data <- Final.Data^2
      newDays.Data <- newDays.Data^2
    } else {
      ap2.Data <- matrix(colSums(Final.Data^2),1,ncol(Final.Data)) 
      ndap2.Data <- matrix(colSums(newDays.Data^2),1,ncol(newDays.Data))
    }
  }

    days <- factor(t(Meyer_2016[c((trunc((newDayWished/daysToRecalculateCluster),1,2) * daysToRecalculateCluster+1):(recalculatedClusterDays + trunc((newDayWished/daysToRecalculateCluster),1,2) * daysToRecalculateCluster)),c(99)]), levels = dayNames,
                   ordered = TRUE)
    Final.DataWD <- rbind(Final.Data,days)
  
  if (newDayWished %% daysToRecalculateCluster == 0) {
    Final.Data <- rollapply( t(Meyer_2016[c((((newDayWished/daysToRecalculateCluster)-1) * daysToRecalculateCluster+1):(recalculatedClusterDays + ((newDayWished/daysToRecalculateCluster)-1) * daysToRecalculateCluster)),-c(97:100)]) , width = sumHowManyColumns , by = sumHowManyColumns , align = "left" , FUN = sum )
    newDays.Data <- rollapply( t(Meyer_2016[c(((recalculatedClusterDays + ((newDayWished/daysToRecalculateCluster)-1) * daysToRecalculateCluster)+1)):nrow(Meyer_2016),-c(97:100)]) , width = sumHowManyColumns , by = sumHowManyColumns , align = "left" , FUN = sum )  
    
    if (aproximation == 2 || aproximation == 3 || aproximation == 5) {
      if(aproximation == 5) {
        Final.Data <- Final.Data^2
        newDays.Data <- newDays.Data^2
      } else {
        ap2.Data <- matrix(colSums(Final.Data^2),1,ncol(Final.Data)) 
        ndap2.Data <- matrix(colSums(newDays.Data^2),1,ncol(newDays.Data))
      }
    }
    
      days <- factor(t(Meyer_2016[c((((newDayWished/daysToRecalculateCluster)-1) * daysToRecalculateCluster+1):(recalculatedClusterDays + ((newDayWished/daysToRecalculateCluster)-1) * daysToRecalculateCluster)),c(99)]), levels = dayNames,
                     ordered = TRUE)
      Final.DataWD <- rbind(Final.Data,days)
    
  }
}

if(option == 2 && daysToRecalculateCluster == 0) {
  Meyer_2016 <-
    read.csv("./Meyer_2016.csv",header=T)
  Final.Data  <- rollapply( t(Meyer_2016[c(1:recalculatedClusterDays),-c(97:100)]) , width = sumHowManyColumns , by = sumHowManyColumns , align = "left" , FUN = sum )
  newDays.Data <- rollapply( t(Meyer_2016[c((recalculatedClusterDays+1):nrow(Meyer_2016)),-c(97:100)]) , width = sumHowManyColumns , by = sumHowManyColumns , align = "left" , FUN = sum )
  
  
  if (aproximation == 2 || aproximation == 3 || aproximation == 5) {
    if(aproximation == 5) {
      Final.Data <- Final.Data^2
      newDays.Data <- newDays.Data^2
    } else {
      ap2.Data <- matrix(colSums(Final.Data^2),1,ncol(Final.Data)) 
      ndap2.Data <- matrix(colSums(newDays.Data^2),1,ncol(newDays.Data))
    }
  }
  
    dayNames <- c("Sunday","Monday", "Tuesday", "Wednesday", 
                  "Thursday", "Friday", "Saturday")
    days <- factor(t(Meyer_2016[c(1:recalculatedClusterDays),c(99)]), levels = dayNames,
                   ordered = TRUE)
    Final.DataWD <- rbind(Final.Data,days)
    newDay <- c()
}

if ( !exists("programAux") ) {
  programAux <- nrow(Meyer_2016) - recalculatedClusterDays - 1
  #programAux <- newDayWished - 1
} else {
  programAux <- programAux - 1
}

#Distance Matrix with lag version
#Specify your data set

dataset <- Final.Data
N<-ncol(dataset) #number of days
Ydist7 <- matrix(0,N,N)
for (j1 in 1:(N-1)){
  for (i1 in (j1+1):N){
    Ydist7[j1,i1]<- as.numeric(Pair.dist4(dataset[,j1],dataset[,i1] , lag=lagFactor)[[1]])
    #distance
  }
}

Ydist7<-as.dist(t(Ydist7))
require(cluster)
require(fpc)

hc7 <- hclust(Ydist7 , "ward.D")
#x11()
#par(mfrow=c(1,1))
#plot(hc7) ##My Part
ward.clust <- cutree(hc7 , k = clusters)
print(ward.clust)
ID<-1:N


#Separate clusters
cList <- list()
counts <- c()
clusterNames <- c()

#Creates the list of clusters
for (i in 1:clusters) {
  clusterNames <- c(clusterNames,paste("Cluster",i,sep=" "))
  
  if (!is.null(nrow(Final.DataWD[,ward.clust==i]))) {
    cList[[i]] <- matrix(data = Final.DataWD[,ward.clust==i], nrow=nrow(Final.DataWD[,ward.clust==i]), ncol=ncol(Final.DataWD[,ward.clust==i]), byrow=F)
  } else {
    cList[[i]] <- matrix(data = Final.DataWD[,ward.clust==i], nrow=minutesDivisions+1, ncol=1, byrow=F)
  }
  
  for (a in 1:length(dayNames)) {
    if (aproximation == 2 || aproximation == 3) {
      counts <- c(counts,length(which(cList[[i]][2,] == a)))
    } else {
      counts <- c(counts,length(which(cList[[i]][minutesDivisions+1,] == a)))
    }
  }
  
}

#Make table for each days in the clusters
counts <- matrix(data = counts, nrow = clusters, ncol = 7, byrow = T)
counts <- as.table(counts)
colnames(counts) <- dayNames
rownames(counts) <- clusterNames
print(counts)

#Create a list with standard deviation of each position on the cluster
cListwithoutIndexes <- list()

#Cluster list without indexes
for (i in 1:clusters) {
  if (aproximation == 2 || aproximation == 3) {
    cListwithoutIndexes[[i]] <- cList[[i]][-c(2),]
  } else {
    cListwithoutIndexes[[i]] <- cList[[i]][-c(minutesDivisions+1),]
  }
}

#Example code to create plots of clusters with medoid highlighted 
#representative day for each cluster
colors <- c("black","red","green","blue")

#Representative day in each cluster (given by the mean of the elements in each cluster)
meanRepresentativeDay <- vector("list", clusters)
representativeDay <- vector("list", clusters)
sd.Data <- vector("list", clusters)


for(i in 1:clusters) {


    if (aproximation == 4 || aproximation == 5) {
        sd.Data[[i]] <- matrix(rowSds(cListwithoutIndexes[[i]]),1,nrow(cListwithoutIndexes[[i]])) #AQUIIIII
        meanRepresentativeDay[[i]] <- matrix(rowMeans(cListwithoutIndexes[[i]]),1,nrow(cListwithoutIndexes[[i]]))
    } else {

    if (!is.null(ncol(cListwithoutIndexes[[i]]))) {
      meanRepresentativeDay[[i]] <- rowMeans(cListwithoutIndexes[[i]][,1:ncol(cList[[i]])])
    }
    else {
      meanRepresentativeDay[[i]] <- cListwithoutIndexes[[i]]
    }
    }

}

distanceToRepresentativeDay <- vector("list", clusters)
distanceToMeanRepresentativeDay <- vector("list", clusters)
meanClusters <- c()
sdClusters <- c()
maxClusters <- c()

#To get in each cluster, the mean, standard deviation and the max value.
for(i in 1:clusters) { 
  if (aproximation == 5) {
    
    meanClusters <- c(meanClusters, meanRepresentativeDay[[i]])
    sdClusters <- c(sdClusters, sd.Data[[i]])
    maxClusters <- c(maxClusters, meanRepresentativeDay[[i]])
    
  } else {
  if (aproximation == 2 || aproximation == 3) {

    if (aproximation == 2) {
      #2 Aproximation
      ap2 <- Final.Data[,ward.clust==i]
      meanClusters <- c(meanClusters, mean(colSums(ap2^2)))
      sdClusters <- c(sdClusters, sd(colSums(ap2^2)))
      maxClusters <- c(maxClusters, max(colSums(ap2^2)))
      
    } else {
      #3 Aproximation
      meanClusters <- c(meanClusters, mean(distanceToRepresentativeDay[[i]]))
      sdClusters <- c(sdClusters, sd(distanceToRepresentativeDay[[i]]))
      maxClusters <- c(maxClusters, max(distanceToRepresentativeDay[[i]]))
      
    }
    
  } else {
    if (aproximation != 4) {
      if (!is.null(ncol(cListwithoutIndexes[[i]]))) {
      for (i1 in 1:ncol(cList[[i]])){
        distanceToRepresentativeDay[[i]][i1] <- as.numeric(Pair.dist4(meanRepresentativeDay[[i]], cListwithoutIndexes[[i]][,i1], lag=lagFactor)[[1]])
        #distance
      }
    }
    
    else {
      distanceToRepresentativeDay[[i]][1] <- as.numeric(Pair.dist4(meanRepresentativeDay[[i]], cListwithoutIndexes[[i]], lag=lagFactor)[[1]])
    }
    
    meanClusters <- c(meanClusters, mean(distanceToRepresentativeDay[[i]]))
    sdClusters <- c(sdClusters, sd(distanceToRepresentativeDay[[i]]))
    maxClusters <- c(maxClusters, max(distanceToRepresentativeDay[[i]]))
    }
  }
}
}

print("New Day(s) Classifications:")

#Check the index of new days in the dataset
if (option == 1) {
  
  if (daysToRecalculateCluster == 0) {
    newDay <- newDays.Data[-c(minutesDivisions+1),auxToNewDay]
  }
  
  else {
    getNewDay <- auxToNewDay%%daysToRecalculateCluster
    
    if( auxToNewDay%%daysToRecalculateCluster == 0) {
      getNewDay <- daysToRecalculateCluster
    }
    newDay <- newDays.Data[-c(minutesDivisions+1),getNewDay]
  }
  
  
} else {
  getNewDay <- c()
  
  if (daysToRecalculateCluster == 0) {
    newDay <- newDays.Data[-c(minutesDivisions+1),newDayWished]
  }
  else {
    getNewDay <- newDayWished%%daysToRecalculateCluster
    
    if( newDayWished%%daysToRecalculateCluster == 0) {
      getNewDay <- daysToRecalculateCluster
    }
    newDay <- newDays.Data[-c(minutesDivisions+1),getNewDay]
  }
}

clusterWarning <- c()


clusterDay <- c()
checkClusterNewDay <- c()
auxToNewDay <- auxToNewDay + 1
newDayIndex <- newDayIndex + 1

for(c in 1:clusters) {
  if(aproximation == 3) {
    checkClusterNewDay[c] <- as.numeric(Pair.dist4(representativeDay[[c]], newDay, lag=lagFactor)[[1]])
  } else {
    if(aproximation == 4 || aproximation == 5){
      checkClusterNewDay[c] <- sum(abs(meanRepresentativeDay[[c]] - newDay))
    } else {
    checkClusterNewDay[c] <- as.numeric(Pair.dist4(meanRepresentativeDay[[c]], newDay, lag=lagFactor)[[1]])
    }
  }
}

clusterDay <- c(clusterDay,which.min(checkClusterNewDay))

if (aproximation == 4 || aproximation == 5) {
  
  correctUp <- meanRepresentativeDay[[clusterDay]] + sd.Data[[clusterDay]]*a45constantUp
  correctDown <- meanRepresentativeDay[[clusterDay]] - sd.Data[[clusterDay]]*a45constantDown
  correctDown[correctDown < 0] <- 0
  superior <- length(newDay[newDay > correctUp])
  inferior <- length(newDay[newDay < correctDown])
  
  if(length(inferior) == 0)
    inferior <- 0
  
  if(length(superior) == 0)
    superior <- 0
  
  if ((superior + inferior) > seriousRange){
    clusterWarning <- c(clusterWarning,("AVISO GRAVE!"))
  } 
  else {
    if ((superior + inferior) > lightRange) {
      clusterWarning <- c(clusterWarning,("AVISO PEQUENO!"))
    }
    else 
      clusterWarning <- c(clusterWarning,("Dia Normal!!"))
  }

  graphShow45(Final.Data,sd.Data,clusterDay,clusterWarning,newDay,correctUp,correctDown,superior,inferior,meanRepresentativeDay)
  
} else {

if (aproximation == 2 || aproximation == 3) {
  newDayAp2 <- sum(newDay^2)
  print("Filho, entraste aqui certo?")
  if ((newDayAp2 > (meanClusters[clusterDay] + sdClusters[clusterDay]*a23constantBig)) || (newDayAp2 < (meanClusters[clusterDay] - a23constantBig*sdClusters[clusterDay]))) {
    clusterWarning <- c(clusterWarning,("AVISO GRAVE!"))
  } else {
    if ((newDayAp2 > (meanClusters[clusterDay] + sdClusters[clusterDay] * a23constantLittle)) || (newDayAp2 < (meanClusters[clusterDay] - sdClusters[clusterDay] * a23constantLittle))) {
      clusterWarning <- c(clusterWarning,("AVISO PEQUENO!"))
    } else {
      clusterWarning <- c(clusterWarning,("Dia Normal!"))
    }
  }
  
  if(aproximation == 3) {
    graphShow23(Final.Data,ward.clust,clusterDay,clusterWarning,representativeDay,colors,newDay,maxClusters,meanClusters,sdClusters,a23constantBig,a23constantLittle)
  } else {
    graphShow23(Final.Data,ward.clust,clusterDay,clusterWarning,meanRepresentativeDay,colors,newDay,maxClusters,meanClusters,sdClusters,a23constantBig,a23constantLittle)
  }
  
  
} else { #Number of Steps
  if (checkClusterNewDay[clusterDay] > (maxClusters[clusterDay] * maxFactor)) {
    clusterWarning <- c(clusterWarning,("AVISO GRAVE!"))
  } else {
    if (checkClusterNewDay[clusterDay] > (meanClusters[clusterDay] + (sdClusters[clusterDay] * sdFactor))) {
      clusterWarning <- c(clusterWarning,("AVISO PEQUENO!"))
    }
    else {
      clusterWarning <- c(clusterWarning,("Dia Normal!"))
    }
  }
  
  graphShow1(Final.Data,ward.clust,clusterDay,clusterWarning,meanRepresentativeDay,colors,newDay)
}
}

helper <- option

if (option == 1 && programAux != 0) {
  lapply(1:programAux, source("./clusters_for_8-2.R")$value)
}
